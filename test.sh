#!/bin/bash

source properties.mk
APPNAME=`grep entry manifest.xml | sed 's/.*entry="\([^"]*\).*/\1/'`

pkill Xvfb
DISPLAY_NUM=49
XAUTHORITY=/tmp/.XAuthority
xvfb-run -n $DISPLAY_NUM -f $XAUTHORITY $SDK_HOME/bin/connectiq &
sleep 3 
$SDK_HOME/bin/monkeydo bin/$APPNAME.prg $DEVICE &
sleep 10
XAUTHORITY=$XAUTHORITY xwd -display :$DISPLAY_NUM -root > /tmp/$APPNAME-$DEVICE.xwd 
convert /tmp/$APPNAME-$DEVICE.xwd ./test_results/$APPNAME-$DEVICE.png
pkill monkeydo
$SDK_HOME/bin/monkeydo bin/$APPNAME.prg $DEVICE -t
pkill simulator
pkill Xvfb 

FROM debian:latest
MAINTAINER Keith Berry "keithwberry@gmail.com"

# ConnectIQ links can be found at: https://developer.garmin.com/downloads/connect-iq/sdks/sdks.json
ENV SDK_VERSION=4.0.7
ENV SDK_URL=https://developer.garmin.com/downloads/connect-iq/sdks/connectiq-sdk-lin-4.0.7-2021-11-29-437ff4cc4.zip
ENV SDK_FILE=sdk.zip
ENV SDK_DIR=/opt/connectiq


#RUN echo "deb http://ftp.us.debian.org/debian/ jessie main" >> /etc/apt/sources.list
#RUN echo "deb http://ftp.us.debian.org/debian/ wheezy main" >> /etc/apt/sources.list
RUN apt-get update -qq && DEBIAN_FRONTEND=noninteractive apt-get install -y wget openjdk-11-jdk unzip build-essential xvfb libusb-1.0-0-dev xorg libpng-dev libwebkit2gtk-4.0-37
RUN apt-get install -y libjpeg-dev imagemagick
RUN wget -O "$SDK_FILE" "$SDK_URL"
RUN unzip "$SDK_FILE" -d "${SDK_DIR}"
RUN chmod 777 ${SDK_DIR}/bin/*
